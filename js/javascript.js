let clicks = 0
let xpoints = 0;
let opoints = 0;
let matrix = new Array(3)
const checkToPoint = (type) => type == 'X' ? xpoints++ : opoints++

fillArray()
function fillArray() {
    for (let i = 0; i < matrix.length; i++) {
        matrix[i] = new Array(3)
        for (let j = 0; j < matrix[i].length; j++) {
            matrix[i][j] = ''
        }
    }
}

function main(number, line, column, id) {
    
    clicks++
    
    let type = ((clicks % 2) == 0) ? 'O' : 'X'
    
    document.game.option[number].innerText = type
    document.getElementById(id).disabled = true
    
    matrix[line][column] = type
    
    if (checkHorizontal(type)) {

        checkToPoint(type)
        
        if (type == 'X') document.getElementById("x").innerText = xpoints
        else document.getElementById("o").innerText = opoints

        setTimeout(function() {
            let ids = document.getElementsByClassName("move")
            for (let i = 0; i < ids.length; i++) {
                document.getElementById(ids[i].id).disabled = false
                document.game.option[i].innerText = ''
            }
            fillArray()
        }, 1000)
    }
}

function checkHorizontal(type) {
    for (let i = 0; i < 3; i++) 
        if ((matrix[i][0] == type) && (matrix[i][1] == type) && (matrix[i][2] == type)) return true
    return checkVertical(type)
}

function checkVertical(type) {
    for (let i = 0; i < 3; i++) 
        if ((matrix[0][i] == type) && (matrix[1][i] == type) && (matrix[2][i] == type)) return true
    return checkDiagonal(type)
}

function checkDiagonal(type) {
    if ((matrix[0][0] == type) && (matrix[1][1] == type) && (matrix[2][2] == type)) return true
    if ((matrix[0][2] == type) && (matrix[1][1] == type) && (matrix[2][0] == type)) return true
    return false
}